#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann (gehrmann.mail@gmail.com)

from __future__ import division, unicode_literals
import os
import sys

if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))

from models.abstract import Memento, ObservableAttrDict


class Settings(ObservableAttrDict):
	def __init__(self):
		self.update(dict(
			# Here can be the default keys&values
		))
		self._memento = memento = Memento('.settings.txt')
		self.update(memento.restore(parse_values=True))
		self.changed.bind(memento.save)

	def __repr__(self):
		return object.__repr__(self)


def main():
	pass

if __name__ == '__main__':
	main()
