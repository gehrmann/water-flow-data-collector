#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann (gehrmann.mail@gmail.com)

from __future__ import division, unicode_literals
import os
import signal; signal.signal(signal.SIGINT, signal.default_int_handler)  # Working Ctrl-C for QT
import sys
import threading
import traceback
import urllib2
import urllib3
# urllib2.install_opener(urllib2.build_opener(urllib2.HTTPHandler(debuglevel=1)))  # urllib2 verbose
# httplib.HTTPConnection.debuglevel = 1  # urllib3 verbose

from PyQt4 import QtCore, QtGui, uic

if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/..'); sys.path.insert(0, os.path.realpath(os.getcwd()))

import controllers.plugins
from helpers.caller import Caller
from models.abstract import ObservableAttrDict, ObservableList
from models.settings import Settings


class _State(ObservableAttrDict):
	pass


class MainController(object):
	_instance = None

	@classmethod
	def init(cls):
		if cls._instance is None:
			cls._instance = cls()
		return cls._instance

	def __init__(self):

		self.network_pool = urllib3.PoolManager(timeout=30.)
		self.network_headers = {'User-agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1'}

		# Models
		self.state_model = state_model = _State()
		state_model.plugins = set(controllers.plugins.__all__)
		state_model.status = 'Press "Start" to (re)run collecting...'
		state_model.log = ObservableList()
		state_model.log.changed.bind(lambda model=None, previous=None, current=None: (state_model.changed(state_model, previous={'log': previous}, current={'log': current})))  # invoke parent's changed-event if is changed
		state_model.exception = None

		self.settings_model = settings_model = Settings()

		# Views
		self.__view = view = uic.loadUi('main_view.ui')

		self.__restore_window_geometry()
		view.closeEvent = self.__on_close
		view.plugins_tree.itemChanged.connect(self.__on_plugins_tree_item_changed)
		view.dst_path_button.clicked.connect(self.__on_dst_path_button_clicked)
		view.results_dst_path_button.clicked.connect(self.__on_results_dst_path_button_clicked)
		view.delay_widget.timeChanged.connect(self.__on_delay_widget_time_changed)
		view.txt_format_checkbox.stateChanged.connect(self.__on_txt_format_checkbox_changed)
		view.csv_format_checkbox.stateChanged.connect(self.__on_csv_format_checkbox_changed)
		view.autostart_checkbox.stateChanged.connect(self.__on_autostart_checkbox_changed)
		view.verbose_checkbox.stateChanged.connect(self.__on_verbose_checkbox_changed)
		view.simulate_response_checkbox.stateChanged.connect(self.__on_simulate_response_checkbox_changed)
		view.start_button.clicked.connect(self.__on_start_button_clicked)
		view.clear_button.clicked.connect(self.__on_clear_button_clicked)

		view.show()

		""" Observe models by view """
		state_model.changed.bind(self._on_model_updated, invoke_in_main_thread=True)
		settings_model.changed.bind(self._on_model_updated, invoke_in_main_thread=True)

		""" Fill blank view by models """
		self._on_model_updated(state_model)
		self._on_model_updated(settings_model)

		if settings_model.get('autostart', False):
			self.__run_process_loop()

		if '--test-main' in sys.argv:
			QtCore.pyqtRemoveInputHook(); import ipdb; ipdb.set_trace()

	""" Model's event handlers """

	def _on_model_updated(self, model=None, previous=None, current=None):
		# print >>sys.stderr, self.__class__, object.__repr__(model), previous and previous, current and current; sys.stderr.flush()  # FIXME: must be removed/commented

		state_model = self.state_model
		settings_model = self.settings_model
		view = self.__view

		if model is state_model:
			if current is None or 'plugins' in current:
				view.plugins_tree.clear()
				for plugin in sorted(state_model.plugins):
					item = QtGui.QTreeWidgetItem([plugin])
					item.setCheckState(0, QtCore.Qt.Unchecked)
					# item.setFlags(QtCore.Qt.ItemIsEnabled)
					view.plugins_tree.addTopLevelItem(item)

			if current is None or 'status' in current:
				view.status_label.setText(state_model.status)

			if current is None or 'log' in current:
				if len(state_model.log) > 1000:  # Keep only last 1000 records
					state_model.log[:] = ['...'] + state_model.log[:1000 - 1]
				else:
					view.log_viewer.setText('\n'.join(state_model.log))

			if current is None or 'exception' in current:
				if state_model.exception is not None:
					QtGui.QMessageBox.warning(self.__view, "Exception", state_model.exception)
					state_model.exception = None

		if model is settings_model:
			if current is None or 'plugins' in current:
				for item in [view.plugins_tree.topLevelItem(x) for x in range(view.plugins_tree.topLevelItemCount())]:
					item.setCheckState(0, QtCore.Qt.Checked if unicode(item.text(0)) in settings_model.setdefault('plugins', state_model.plugins) else QtCore.Qt.Unchecked)

			if current is None or 'dst_path' in current:
				view.dst_path_button.setText(settings_model.setdefault('dst_path', './data.txt'))

			if current is None or 'results_dst_path' in current:
				view.results_dst_path_button.setText(settings_model.setdefault('results_dst_path', ''))

			if current is None or 'delay' in current:
				view.delay_widget.setTime(QtCore.QTime().addSecs(settings_model.setdefault('delay', 15 * 60)))

			if current is None or 'txt_format' in current:
				view.txt_format_checkbox.setChecked(settings_model.setdefault('txt_format', False))

			if current is None or 'csv_format' in current:
				view.csv_format_checkbox.setChecked(settings_model.setdefault('csv_format', False))

			if current is None or 'autostart' in current:
				view.autostart_checkbox.setChecked(settings_model.setdefault('autostart', False))

			if current is None or 'verbose' in current:
				view.verbose_checkbox.setChecked(settings_model.setdefault('verbose', False))

			if current is None or 'simulate_response' in current:
				view.simulate_response_checkbox.setChecked(settings_model.setdefault('simulate_response', False))

	""" View's event handlers """

	def __on_close(self, event=None):
		with open('.geometry', 'w') as storage:
			print >>storage, self.__view.saveGeometry()

	def __on_plugins_tree_item_changed(self, item, column):
		getattr(self.settings_model.plugins, 'add' if item.checkState(0) == QtCore.Qt.Checked else 'discard')(unicode(item.text(0)))
		self.settings_model.changed(self.settings_model)  # Invoke parent's changed-event if is changed

	def __on_dst_path_button_clicked(self):
		self.settings_model.dst_path = unicode(QtGui.QFileDialog.getSaveFileName(parent=self.__view, caption="Select Path", directory=self.settings_model.dst_path, filter="Text files, *.txt (*.txt);; Databases, *.csv (*.csv)"))

	def __on_results_dst_path_button_clicked(self):
		self.settings_model.results_dst_path = unicode(QtGui.QFileDialog.getSaveFileName(parent=self.__view, caption="Select Path", directory=self.settings_model.results_dst_path, filter="Text files, *.txt (*.txt);; Databases, *.csv (*.csv)"))

	def __on_delay_widget_time_changed(self, value):
		self.settings_model.delay = value.hour() * 3600 + value.minute() * 60 + value.second()

	def __on_txt_format_checkbox_changed(self):
		self.settings_model.txt_format = self.__view.txt_format_checkbox.isChecked()

	def __on_csv_format_checkbox_changed(self):
		self.settings_model.csv_format = self.__view.csv_format_checkbox.isChecked()

	def __on_autostart_checkbox_changed(self):
		self.settings_model.autostart = self.__view.autostart_checkbox.isChecked()

	def __on_verbose_checkbox_changed(self):
		self.settings_model.verbose = self.__view.verbose_checkbox.isChecked()

	def __on_simulate_response_checkbox_changed(self):
		self.settings_model.simulate_response = self.__view.simulate_response_checkbox.isChecked()

	def __on_start_button_clicked(self):
		self.__run_process_loop()

	def __on_clear_button_clicked(self):
		self.state_model.log[:] = []

	""" Helpers """

	def __restore_window_geometry(self):
		try:
			with open('.geometry') as storage:
				self.__view.restoreGeometry(storage.read())
		except IOError:
			pass

	def __run_process_loop(self):
		Caller.call_once_after(0., self.__run_process)

	def __run_process(self):
		try:
			def run():
				try:
					for plugin_name in sorted(self.state_model.plugins & self.settings_model.plugins):
						plugin_module = getattr(controllers.plugins, plugin_name)
						plugin = plugin_module.Plugin(controller=self)
						plugin.run()
				except Exception as e:
					self.state_model.exception = "{}\n\n{}".format(e, traceback.format_exc())

			thread = threading.Thread(target=run)
			thread.setDaemon(True)
			thread.start()

			Caller.call_once_after(max(self.settings_model.delay, 10), self.__run_process)  # Call after delay (min after 10s)

		except Exception as e:
			self.state_model.exception = "{}\n\n{}".format(e, traceback.format_exc())

	def get_response(self, url, method='GET', headers=None, codepage='UTF-8', save_to_storage=False, storage='.cache/tmp.cache'):
		if self.settings_model.simulate_response:  # Restore from cache (for debugging without access to sensors)
			with open(storage, 'rb') as src:
				msg = src.read()
				headers, data = msg.split(b'\r\n\r\n', 1)
				headers = dict([line.split(': ') for line in unicode(headers, codepage).split('\r\n')])

		else:
			response = self.network_pool.request(method=method, url=url, headers=(headers if headers is not None else self.network_headers))
			headers, data = response.getheaders(), response.data

			if save_to_storage:  # Save to cache
				with open(storage, 'wb') as src:
					# src.write(data.encode('UTF-8'))
					src.write('\r\n'.join(['{0}: {1}'.format(key, value) for key, value in headers.items()]))
					src.write('\r\n\r\n')
					src.write(data)

		if headers.get('Content-Type', '') == 'application/octet-stream':
			import zlib
			data = zlib.decompress(data, 16 + zlib.MAX_WBITS)

		data = unicode(data, codepage)

		return headers, data


def run_MainController():
	app = QtGui.QApplication(sys.argv)

	MainController.init()

	sys.exit(app.exec_())


def run_MainController_get_response():
	app = QtGui.QApplication(sys.argv)

	controller = MainController.init()
	controller.get_response(url='http://192.168.1.11/Login', save_to_storage=True, codepage='iso-8859-1', storage='.cache/controllers.plugins.NIVUS_OCM_Pro-__log_in-1.cache')


def main():
	# run_MainController()
	run_MainController_get_response()

if __name__ == '__main__':
	main()
