# -*- coding: utf-8 -*-
# vim: noexpandtab

import os

__all__ = [
	os.path.splitext(x)[0]
	for x in os.listdir(os.path.join('controllers', 'plugins'))
	if all((
			os.path.isfile(os.path.join('controllers', 'plugins', x)),
			not x.startswith('__'),
			x.endswith('.py'),
	))
]

for module in __all__:
	__import__('controllers.plugins.{module}'.format(**locals()))
