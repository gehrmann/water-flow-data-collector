#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann (gehrmann.mail@gmail.com)

from __future__ import division, unicode_literals
import datetime
import os
import signal; signal.signal(signal.SIGINT, signal.default_int_handler)  # Working Ctrl-C for QT
import sys

if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/../..'); sys.path.insert(0, os.path.realpath(os.getcwd()))

import __SC1000


class Plugin(__SC1000.Plugin):
	def __init__(self, *args, **kwargs):
		super(Plugin, self).__init__(*args, **kwargs)

		self._log_file_parameter = 'H02'
		self._log_type_parameter = 'H02D'
		self._log_channels = (
			'&Channel0=H02C00'  # Checkbox: OXYGEN
			'&Channel1=H02C01'  # Checkbox: TEMPERATUR
		)
