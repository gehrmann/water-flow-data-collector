#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann (gehrmann.mail@gmail.com)

from __future__ import division, unicode_literals
from collections import defaultdict
import datetime
import errno
import os
import re
import signal; signal.signal(signal.SIGINT, signal.default_int_handler)  # Working Ctrl-C for QT
import sys
import traceback
import urllib3

from PyQt4 import QtGui


if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/../..'); sys.path.insert(0, os.path.realpath(os.getcwd()))


class Plugin(object):
	def __init__(self, controller):
		self.controller = controller
		self._results_dst_diff_enabled = True

	def run(self):
		state_model = self.controller.state_model
		settings_model = self.controller.settings_model

		verbose = settings_model.verbose
		dst_path = settings_model.dst_path
		results_dst_path = settings_model.results_dst_path
		csv_format = settings_model.csv_format
		txt_format = settings_model.txt_format

		state_model.log += [datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S ')]

		try:
			state_model.status = (self.__class__.__module__.split('.'))[-1] + ':  ' + 'filling with new data...'
			for data_format in (['csv'] if csv_format else []) + (['txt'] if txt_format else []):
				for message in self._append_new_data(
						data_format=data_format,
						dst_path=os.path.splitext(dst_path)[0] + '.' + data_format,
						results_dst_path=os.path.splitext(results_dst_path)[0] + '.' + data_format,
						verbose=verbose,
						simulate_response=settings_model.simulate_response,
				):
					state_model.log += ['{}: {}'.format(__name__, message)]

			state_model.log += ['{}: Done'.format(__name__)]
		except IOError as e:
			state_model.log += ['Exception: {}'.format(e.args[1] if hasattr(e, 'args') and len(e.args) >= 2 else str(e))]
			if verbose:
				state_model.exception = '{}\n\n{}'.format(e, traceback.format_exc())
		except Exception as e:
			raise

		state_model.status = ''
		state_model.log += [('\n--\n' if verbose else ' ')]

	# @line_profiler
	def _append_new_data(self, data_format, dst_path, results_dst_path=None, verbose=False, simulate_response=False):
		src_re_time_format = r'^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}'
		src_datetime_format = '%Y-%m-%d %H:%M:%S'
		src_time_length = 19
		dst_re_time_format = r'^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}'
		dst_datetime_format = '%Y-%m-%d %H:%M:%S'
		dst_time_length = 19

		# Check if directory still exists (The remote storage is still reachable)
		if verbose:
			yield 'checking if destination path exists...'
		self._check_if_path_exists(os.path.dirname(dst_path))
		if verbose:
			yield 'destination path exists'

		if results_dst_path:
			# Check if directory still exists (The remote storage is still reachable)
			if verbose:
				yield 'checking if remote destination path exists...'
			self._check_if_path_exists(os.path.dirname(results_dst_path))
			if verbose:
				yield 'remote destination path exists'

		# Find dst's latest time
		if verbose:
			yield 'calculating latest time on destination...'
		try:
			with open(dst_path) as dst:
				latest_time_string = next(x for x in reversed(dst.readlines()) if re.search(dst_re_time_format, x))[:dst_time_length]
		except (IOError, StopIteration):
			latest_time_string = datetime.datetime.combine(datetime.datetime.now() - datetime.timedelta(days=(9999 if simulate_response else 1)), datetime.time(0, 0, 0)).strftime(dst_datetime_format)
		latest_time = datetime.datetime.strptime(latest_time_string, dst_datetime_format)
		latest_time_string = datetime.datetime.strftime(latest_time, src_datetime_format)
		if verbose:
			yield 'calculated latest time on destination: {}'.format(latest_time)

		src_paths = sorted([
			os.path.join(os.path.dirname(dst_path), x)
			for x in os.listdir(os.path.dirname(dst_path))
			if x.startswith(os.path.splitext(os.path.basename(dst_path))[0] + '_') and x.endswith(data_format)
		])
		if verbose:
			yield 'Source paths: {}'.format(src_paths)

		# Check if directory still exists (The remote storage is still reachable)
		self._check_if_path_exists(os.path.dirname(dst_path))

		# Write headers if not exists
		keys = []
		for src_path in src_paths:
			if os.path.exists(src_path):
				with open(src_path) as src:
					for line in src:
						if re.search(src_re_time_format, line):
							break
						if line.startswith('Datum'):
							line = line.rstrip()
							keys.extend(line[((len('Datum') + 1) if keys else 0):].split(',' if data_format == 'csv' else '\t'))
		if not os.path.exists(dst_path) or os.path.getsize(dst_path) == 0:
			with open(dst_path, 'w') as dst:
				print >>dst, (',' if data_format == 'csv' else '\t').join(keys)

		if results_dst_path and self._results_dst_diff_enabled:
			if verbose:
				yield 'Saving previous results (to make difference)...'
			with open(dst_path) as src:
				with open(dst_path + '-prev', 'w') as dst:
					dst.write(src.read())
			if verbose:
				yield 'Done.'

		if verbose:
			yield 'Collecting data...'
		data = defaultdict(lambda: [])
		for src_path in src_paths:
			if os.path.exists(src_path):
				_latest_time = latest_time
				with open(src_path) as src:
					for line in src:
						line_time_string = line[:src_time_length]
						if re.search(src_re_time_format, line_time_string):
							if line_time_string > latest_time_string:
								line_time = datetime.datetime.strptime(line[:src_time_length], src_datetime_format)
								_latest_time = line_time
								line_data = line[src_time_length + 1:]
								if len(line) > src_time_length:  # Skip lines with only time
									line_data = line_data.rstrip()
									# print >>dst, line_time.strftime(dst_datetime_format) + line_data.rstrip()
									# print line_time.strftime(dst_datetime_format) + line_data.rstrip()
									data[line_time.strftime(dst_datetime_format)].extend(line_data.split(',' if data_format == 'csv' else '\t'))
		if verbose:
			yield 'Writing into destination path...'
		# Write new data from srcs to dst
		with open(dst_path, 'a') as dst:
			for key in sorted(data.keys()):
				if len(keys) == len(data[key]) + 1:
					print >>dst, (',' if data_format == 'csv' else '\t').join([key] + data[key])
		if verbose:
			yield 'Done.'

		# Copy the results to a separate directory (in order to ease the synchronization of active-directory with WinSCP)
		# results_dst_path = os.path.join(os.path.dirname(dst_path), 'results', os.path.basename(dst_path))
		if results_dst_path:
			if verbose:
				yield 'Creating remote directory...'
			try:
				os.mkdir(os.path.dirname(results_dst_path))
			except OSError as e:
				if e.errno != errno.EEXIST:
					raise
			if verbose:
				yield 'Done.'

			src_offset = 0
			if self._results_dst_diff_enabled:
				src_offset = os.path.getsize(dst_path + '-prev')
			if verbose:
				yield 'Data offset to upload: {}'.format(src_offset)

			if verbose:
				yield 'Writing into remote destination path...'
			with open(dst_path) as src:
				src.seek(src_offset)
				_results_dst_path = (
					os.path.splitext(results_dst_path)[0] +
					(datetime.datetime.now().strftime('_%Y-%m-%d-%H-%M-%S') if self._results_dst_diff_enabled else '') +
					os.path.splitext(results_dst_path)[1]
				)
				with open(_results_dst_path, 'w') as dst:
					dst.write(src.read())
			if verbose:
				yield 'Done.'

	def _check_if_path_exists(self, path):
		if not os.path.exists(path):
			raise IOError('Path is unreachable: {}'.format(path))


def run_append_new_data():
	device = Plugin(controller=None)
	# for data_format in ['txt']:
	for data_format in ['txt', 'csv']:
		for message in device._append_new_data(data_format=data_format, dst_path='.data/data.' + data_format, verbose=1, simulate_response=True):
			print message


def main():
	run_append_new_data()

if __name__ == '__main__':
	main()
