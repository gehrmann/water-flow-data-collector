#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann (gehrmann.mail@gmail.com)

from __future__ import division, unicode_literals
import datetime
import os
import re
import signal; signal.signal(signal.SIGINT, signal.default_int_handler)  # Working Ctrl-C for QT
import sys
import time
import traceback
import urllib3

if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/../..'); sys.path.insert(0, os.path.realpath(os.getcwd()))


class Plugin(object):
	def __init__(self, controller):
		self._controller = controller
		self._settings_model = settings_model = controller.settings_model
		self._state_model = state_model = controller.state_model

		self._ip = '192.168.1.10'
		self._login_password = '12345'
		self._wait_till_data_ready_timeout = 60
		self._log_file_parameter = 'H02'
		self._log_type_parameter = 'H04D'
		self._log_channels = (
			'&Channel0=H04C00'  # Checkbox: AMMONIUM
			'&Channel1=H04C01'  # Checkbox: NITRAT
			'&Channel2=H04C02'  # Checkbox: KALIUM
			'&Channel3=H04C03'  # Checkbox: CHLORID
			'&Channel4=H04C04'  # Checkbox: TEMPERATUR
			# '&Channel5=H04C05'  # Checkbox: NH4 MV
			# '&Channel6=H04C06'  # Checkbox: NO3 MV
			# '&Channel7=H04C07'  # Checkbox: K+ MV
			# '&Channel8=H04C08'  # Checkbox: CL- MV
			# '&Channel9=H04C09'  # Checkbox: REF1 MV
			# '&Channel10=H04C10'  # Checkbox: REF2 MV
			# '&Channel11=H04C11'  # Checkbox: GNDROD
		)

	def _log(self, message):
		settings_model = self._settings_model
		state_model = self._state_model

		if settings_model.verbose:
			state_model.log += [str(message)]

	def run(self):
		settings_model = self._settings_model
		state_model = self._state_model

		self._log(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S "))
		try:
			state_model.status = (self.__class__.__module__.split('.'))[-1] + ':  ' + 'logging in...'
			for message in self.__log_in():
				self._log(message)

			state_model.status = (self.__class__.__module__.split('.'))[-1] + ':  ' + 'waiting data readiness...'
			for message in self.__wait_till_data_ready():
				self._log(message)

			for data_format in (['csv'] if settings_model.csv_format else []) + (['txt'] if settings_model.txt_format else []):
				state_model.status = (self.__class__.__module__.split('.'))[-1] + ':  ' + 'loading data...'
				for message in self.__load_data(
						data_format=data_format,
						dst_path=(os.path.splitext(settings_model.dst_path)[0] + '_' + (self.__class__.__module__.split('.')[-1]) + '.' + data_format),
						simulate_response=settings_model.simulate_response,
				):
					self._log(message)

			self._log('Done')
		except (urllib3.exceptions.HTTPError, IOError) as e:
			state_model.status = (self.__class__.__module__.split('.'))[-1] + ':  ' + 'exception'
			self._log('Exception: {}'.format(e.args[1] if hasattr(e, 'args') and len(e.args) >= 2 else str(e)))
			if settings_model.verbose:
				state_model.exception = "{}\n\n{}".format(e, traceback.format_exc())
		except Exception as e:
			state_model.status = (self.__class__.__module__.split('.'))[-1] + ':  ' + 'exception'
			raise

		state_model.status = ''

	def __log_in(self):
		settings_model = self._settings_model

		url = getattr(self, '_log_in_url', (
			'http://{self._ip}/cgi-bin/SC1000?'
			'Mode=1'
			'&password={self._login_password}'
			'&SetLanguage=1'
		)).format(**locals())
		if settings_model.verbose:
			yield url

		response_headers, response_data = self._controller.get_response(
			url=url,
			save_to_storage=settings_model.verbose,
			storage='.cache/{0}-{1}.cache'.format(self.__module__, '__log_in'),
		)
		if settings_model.verbose:
			yield response_headers
			yield response_data

	def __wait_till_data_ready(self, timeout=60):
		"""Requests URL with Mode=30 and waits till Mode=31"""
		settings_model = self._settings_model

		url = getattr(self, '_wait_till_data_ready_url', (
			'http://{self._ip}/cgi-bin/SC1000?'
			'&Mode=30'
			'&LogFile={self._log_file_parameter}'
			'&UserID=1'
			'&DirectLink=1'
			'&AUTO_REFRESH=1'
		)).format(**locals())
		if settings_model.verbose:
			yield url

		# Waits for readiness status
		attempts = self._wait_till_data_ready_timeout
		for attempt in range(attempts):
			response_headers, response_data = self._controller.get_response(
				url=url,
				save_to_storage=settings_model.verbose,
				storage='.cache/{0}-{1}.cache'.format(self.__module__, '__wait_till_data_ready'),
			)
			if settings_model.verbose:
				yield response_headers
				yield response_data
			# Looks for readiness status (&Mode=30 = busy, &Mode=31 = ready)
			if 'Mode=31' in response_data:
				break
			# Waits 1s
			time.sleep(1)
		else:
			raise Exception('Data not ready after {self._wait_till_data_ready_timeout}s.'.format(**locals()))

	def __load_data(self, data_format, dst_path, simulate_response=False):
		settings_model = self._settings_model

		src_re_time_format = r'^\d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}'
		src_datetime_format = '%Y/%m/%d %H:%M:%S'
		src_time_length = 19
		dst_re_time_format = r'^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}'
		dst_datetime_format = '%Y-%m-%d %H:%M:%S'
		dst_time_length = 19

		# Check if directory still exists (The remote storage is still reachable)
		self._check_if_path_exists(os.path.join(*os.path.split(dst_path)[:-1]))

		# Find dst's latest time
		try:
			with open(dst_path) as dst:
				# Accelerated search of last time
				dst.seek(0, os.SEEK_END)  # Fast-forward to the end of file
				dst.seek(max(dst.tell() - 1024, 0), 0)  # Backwards to position = -1024
				latest_time_string = next(x for x in reversed(dst.readlines()) if re.search(dst_re_time_format, x))[:dst_time_length]
		except (IOError, StopIteration):
			latest_time_string = datetime.datetime.combine(datetime.datetime.now() - datetime.timedelta(days=(9999 if simulate_response else 0)), datetime.time(0, 0, 0)).strftime(dst_datetime_format)
		latest_time = datetime.datetime.strptime(latest_time_string, dst_datetime_format)
		latest_time_string = datetime.datetime.strftime(latest_time, src_datetime_format)
		if settings_model.verbose:
			yield latest_time
		current_time = datetime.datetime.now()

		from_timestamp = int(time.mktime(latest_time.timetuple()))
		to_timestamp = int(time.mktime(current_time.timetuple()))
		_data_format = dict(csv=0, txt=1)[data_format]

		# Create request to server
		url = getattr(self, '_load_data_url', (
			'http://{self._ip}/cgi-bin/SC1000?'
			'Mode=98'
			'&UserID=1'
			'&LogFile={self._log_file_parameter}'
			'&LogType={self._log_type_parameter}'  # Radio: [ EVENTLOGGER=H04E | DATENLOGGER=H04D ]

			'{self._log_channels}'

			'&Range=d'  # Radio: [ ALLE=a | LETZTER TAG=d | LETZTE WOCHE=w | LETZTER MONAT=m | ZEITRAUM DATENLOGGER=X | ZEITRAUM EVENTLOGGER=x ]
			# '&DateStartD={from_timestamp}'  # Dropdown: ZEITRAUM DATENLOGGER [ UNIX TIMESTAMP 00:00:00 ]
			# '&DateEndD={to_timestamp}'  # Dropdown: ZEITRAUM DATENLOGGER [ UNIX TIMESTAMP 23:59:59 ]
			# '&DateStartE={from_timestamp}'  # Dropdown: ZEITRAUM EVENTLOGGER [ UNIX TIMESTAMP 00:00:00 ]
			# '&DateEndE={to_timestamp}'  # Dropdown: ZEITRAUM EVENTLOGGER [ UNIX TIMESTAMP 23:59:59 ]
			'&TxtFormat={_data_format}'  # Radio: [ CSV=0 | TXT=1 ]
			'&Compression=0'  # Checkbox: [ GZIP=1 ]
		)).format(**locals())
		if settings_model.verbose:
			yield url

		# Send request
		response_headers, response_data = self._controller.get_response(
			url=url,
			save_to_storage=settings_model.verbose,
			storage='.cache/{0}-{1}-{2}.cache'.format(self.__module__, '__load_data', data_format),
		)
		if settings_model.verbose:
			yield response_headers
			yield response_data[:200]
			yield response_data[-100:]

		# Perhaps a bug in SC1000: urllib3 finds only part of headers, some headers remain in "body"
		# FIX:
		# broken_headers, data = response_data.split('\r\n\r\n', 1)
		broken_headers, data = '', ''
		for i in range(len(response_data)):
			if response_data[i:i + 4] == '\r\n\r\n':
				broken_headers, data = response_data[:i], response_data[i + 4:].splitlines()
				break

		# Check if directory still exists (The remote storage is still reachable)
		self._check_if_path_exists(os.path.join(*os.path.split(dst_path)[:-1]))

		# Write headers if not exists
		if not os.path.exists(dst_path) or os.path.getsize(dst_path) == 0:
			# print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "\n--------------------\nOpening ", dst_path; sys.stderr.flush()  # FIXME: must be removed/commented
			with open(dst_path, 'w') as dst:
				for line in data:
					if re.search(src_re_time_format, line):
						break
					if line.startswith('DATUM'):
						# print '>> ', line
						print >>dst, line.replace('DATUM', 'Datum', 1)

		# print >>sys.stderr, '{0.f_code.co_filename}:{0.f_lineno}:'.format(sys._getframe()), "\n--------------------\nOpening ", dst_path; sys.stderr.flush()  # FIXME: must be removed/commented
		with open(dst_path, 'a') as dst:
			for line in data:
				line_time_string = line[:src_time_length]
				if re.search(src_re_time_format, line_time_string):
					if line_time_string > latest_time_string:
						line_time = datetime.datetime.strptime(line[:src_time_length], src_datetime_format)
						latest_time = line_time
						line_data = line[src_time_length:]
						if len(line) > src_time_length:  # Skip lines with only time
							# print '>> ', line_time.strftime(dst_datetime_format) + line_data
							print >>dst, line_time.strftime(dst_datetime_format) + line_data
		# sys.exit()  # FIXME: must be removed/commented

	def _check_if_path_exists(self, path):
		if not os.path.exists(path):
			raise IOError("Path is unreachable: {}".format(path))


def run_list_controller():
	settings_model = ObservableAttrDict(
		list_vocabulary='Körperteile',
		list_font=None,
	)

	controller = Plugin(settings_model=settings_model)
	# controller._view.setGeometry(850, 50, 480, 640)

	# import ipdb; ipdb.set_trace()  # FIXME: must be removed
	sys.exit(app.exec_())


def main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', default='list_controller', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
