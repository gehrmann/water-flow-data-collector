#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann (gehrmann.mail@gmail.com)

from __future__ import division, unicode_literals
import datetime
import os
import signal; signal.signal(signal.SIGINT, signal.default_int_handler)  # Working Ctrl-C for QT
import sys

if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/../..'); sys.path.insert(0, os.path.realpath(os.getcwd()))

import __SC1000


class Plugin(__SC1000.Plugin):
	def __init__(self, *args, **kwargs):
		super(Plugin, self).__init__(*args, **kwargs)

		self._log_file_parameter = 'H02'
		self._log_type_parameter = 'H04D'
		self._log_channels = (
			'&Channel0=H04C00'  # Checkbox: AMMONIUM
			'&Channel1=H04C01'  # Checkbox: NITRAT
			'&Channel2=H04C02'  # Checkbox: KALIUM
			'&Channel3=H04C03'  # Checkbox: CHLORID
			'&Channel4=H04C04'  # Checkbox: TEMPERATUR
			# '&Channel5=H04C05'  # Checkbox: NH4 MV
			# '&Channel6=H04C06'  # Checkbox: NO3 MV
			# '&Channel7=H04C07'  # Checkbox: K+ MV
			# '&Channel8=H04C08'  # Checkbox: CL- MV
			# '&Channel9=H04C09'  # Checkbox: REF1 MV
			# '&Channel10=H04C10'  # Checkbox: REF2 MV
			# '&Channel11=H04C11'  # Checkbox: GNDROD
		)
