#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann (gehrmann.mail@gmail.com)

from __future__ import division, unicode_literals
import datetime
import os
import re
import signal; signal.signal(signal.SIGINT, signal.default_int_handler)  # Working Ctrl-C for QT
import sys
import traceback
import urllib3

if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/../..'); sys.path.insert(0, os.path.realpath(os.getcwd()))


class Plugin(object):
	def __init__(self, controller):
		self._controller = controller
		self._settings_model = settings_model = controller.settings_model
		self._state_model = state_model = controller.state_model

		self._directory_name = 'ORIGINAL'

	def _log(self, message):
		settings_model = self._settings_model
		state_model = self._state_model

		if settings_model.verbose:
			state_model.log += [str(message)]

	def run(self):
		state_model = self._state_model
		settings_model = self._settings_model

		self._log(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S "))
		try:
			state_model.status = (self.__class__.__module__.split('.'))[-1] + ':  ' + 'filling with new data...'
			for data_format in (['csv'] if settings_model.csv_format else []) + (['txt'] if settings_model.txt_format else []):
				for message in self._append_new_data(
						data_format=data_format,
						dst_path=os.path.splitext(settings_model.dst_path)[0] + '_' + (self.__class__.__module__.split('.')[-1]) + '.' + data_format,
						simulate_response=settings_model.simulate_response,
				):
					self._log(message)

			self._log('Done')
		except IOError as e:
			self._log('Exception: {}'.format(e.args[1] if hasattr(e, 'args') and len(e.args) >= 2 else str(e)))
			if settings_model.verbose:
				state_model.exception = "{}\n\n{}".format(e, traceback.format_exc())
		except Exception as e:
			raise

		state_model.status = ''

	def _append_new_data(self, data_format, dst_path, simulate_response=False):
		settings_model = self._settings_model

		src_file_datetime_format = '%Y-%m-%d_%H-%M-%S.par'
		src_re_time_format = r'^\d{4}\.\d{2}\.\d{2}  \d{2}:\d{2}:\d{2}'
		src_datetime_format = '%Y.%m.%d  %H:%M:%S'
		src_time_length = 20
		dst_re_time_format = r'^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}'
		dst_datetime_format = '%Y-%m-%d %H:%M:%S'
		dst_time_length = 19
		delimiter = ',' if data_format == 'csv' else '\t'
		columns_indices = [0, 2, 4, 6, 8, 10, 12]
		src_path = ('.cache/' if simulate_response else 'C:/') + 's-canV5.0/Results/{self._directory_name}'.format(**locals())
		encoding = 'latin-1'

		# Check if directory still exists (The remote storage is still reachable)
		self._check_if_path_exists(os.path.join(*os.path.split(dst_path)[:-1]))

		# Find dst's latest time
		try:
			with open(dst_path) as dst:
				latest_time_string = next(x for x in reversed(dst.readlines()) if re.search(dst_re_time_format, x))[:dst_time_length]
		except (IOError, StopIteration):
			latest_time_string = datetime.datetime.combine(datetime.datetime.now() - datetime.timedelta(days=(9999 if simulate_response else 0)), datetime.time(0, 0, 0)).strftime(dst_datetime_format)
		latest_time = datetime.datetime.strptime(latest_time_string, dst_datetime_format)
		latest_time_string = datetime.datetime.strftime(latest_time, src_datetime_format)
		if settings_model.verbose:
			yield latest_time

		# Get all src's timestamps
		src_times = sorted([datetime.datetime.strptime(x, src_file_datetime_format) for x in os.listdir(src_path) if re.search(r'^\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}.par$', x)])

		# Get all fresh and last two outdated
		src_times = src_times[- 2 - len([x for x in src_times if x > latest_time]):]

		# Check if directory still exists (The remote storage is still reachable)
		self._check_if_path_exists(os.path.join(*os.path.split(dst_path)[:-1]))

		# Write headers if not exists
		keys = []
		for src_time in src_times[:1]:
			src_filename = src_time.strftime(src_file_datetime_format)
			# print >>dst, '\nFROM {}\n'.format(src_filename)
			with open(os.path.join(src_path, src_filename)) as src:
				for line in src:
					line = line.decode(encoding)
					if re.search(src_re_time_format, line):
						break
					if line.startswith('Date/Time'):
						line = line.rstrip().replace('Date/Time', 'Datum', 1).replace(',', '.')
						keys.extend(line.split('\t'))
		if not os.path.exists(dst_path) or os.path.getsize(dst_path) == 0:
			with open(dst_path, 'w') as dst:
				print >>dst, delimiter.join([keys[column] for column in columns_indices])

		# Write new data from srcs to dst
		with open(dst_path, 'a') as dst:
			for src_time in src_times:
				src_filename = src_time.strftime(src_file_datetime_format)
				# print >>dst, '\nFROM {}\n'.format(src_filename)
				if settings_model.verbose:
					yield 'From {}'.format(os.path.join(src_path, src_filename))
				with open(os.path.join(src_path, src_filename)) as src:
					for line in src:
						line = line.decode(encoding)
						line_time_string = line[:src_time_length]
						if re.search(src_re_time_format, line_time_string):
							if line_time_string > latest_time_string:
								line_time = datetime.datetime.strptime(line[:src_time_length], src_datetime_format)
								latest_time = line_time
								line_values = line[src_time_length:]
								if len(line) > src_time_length:  # Skip lines with only time
									line = line_time.strftime(dst_datetime_format) + line_values.rstrip().replace(',', '.')
									data = line.split('\t')
									print >>dst, delimiter.join([data[column] for column in columns_indices])

	def _check_if_path_exists(self, path):
		if not os.path.exists(path):
			raise IOError("Path is unreachable: {}".format(path))


def run_append_new_data():
	device = Plugin(controller=None)
	for data_format in ['txt', 'csv']:
		print data_format
		for message in device._append_new_data(data_format=data_format, dst_path='.data/S_Can.' + data_format, simulate_response=True):
			print message


def main():
	run_append_new_data()

if __name__ == '__main__':
	main()
