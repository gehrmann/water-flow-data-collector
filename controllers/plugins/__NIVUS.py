#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann (gehrmann.mail@gmail.com)

from __future__ import division, unicode_literals
import datetime
import os
import re
import signal; signal.signal(signal.SIGINT, signal.default_int_handler)  # Working Ctrl-C for QT
import sys
import traceback
import urllib3

if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/../..'); sys.path.insert(0, os.path.realpath(os.getcwd()))


class Plugin(object):
	def __init__(self, controller):
		self._controller = controller
		self._settings_model = settings_model = controller.settings_model
		self._state_model = state_model = controller.state_model

		self._username = 'admin'
		self._password = 'nivus'
		self._load_data_script = 'NIVUS'

	def _log(self, message):
		settings_model = self._settings_model
		state_model = self._state_model

		if settings_model.verbose:
			state_model.log += [str(message)]

	def run(self):
		state_model = self._state_model
		settings_model = self._settings_model

		self._log(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S "))
		try:
			state_model.status = (self.__class__.__module__.split('.'))[-1] + ':  ' + 'logging in...'
			for message in self.__log_in():
				self._log(message)

			for data_format in (['csv'] if settings_model.csv_format else []) + (['txt'] if settings_model.txt_format else []):
				state_model.status = (self.__class__.__module__.split('.'))[-1] + ':  ' + 'loading data...'
				for message in self.__load_data(
						data_format=data_format,
						dst_path=os.path.splitext(settings_model.dst_path)[0] + '_' + (self.__class__.__module__.split('.')[-1]) + '.' + data_format,
						simulate_response=settings_model.simulate_response,
				):
					self._log(message)

			self._log('Done')
		except (_ProcessingException, urllib3.exceptions.HTTPError, IOError) as e:
			state_model.status = (self.__class__.__module__.split('.'))[-1] + ':  ' + 'exception'
			self._log('Exception: {}'.format(e.args[1] if hasattr(e, 'args') and len(e.args) >= 2 else str(e)))
			if settings_model.verbose:
				state_model.exception = "{}\n\n{}".format(e, traceback.format_exc())
		except Exception as e:
			state_model.status = (self.__class__.__module__.split('.'))[-1] + ':  ' + 'exception'
			raise

		state_model.status = ''

	def __log_in(self):
		settings_model = self._settings_model

		for attempts in range(2, -1, -1):
			url = 'http://192.168.1.11/Login'
			if settings_model.verbose:
				yield url

			response_headers, response_data = self._controller.get_response(url=url, codepage='iso-8859-1', save_to_storage=settings_model.verbose, storage='.cache/{0}-{1}.cache'.format(self.__module__, '__log_in-1'))
			if settings_model.verbose:
				yield response_headers
				yield response_data[:2000]
			challenge = next(x for x in response_data.splitlines() if 'var challenge="' in x).split('var challenge="')[1].split('"')[0]
			if settings_model.verbose:
				yield challenge

			url = (
				'http://192.168.1.11/Login?'
				'UN={self._username}'
				'&PW={encrypted_password}'
			).format(
				encrypted_password=_PasswordEncoder.hex_hmac_md5(self._password, challenge),
				**locals()
			)

			if settings_model.verbose:
				yield url

			response_headers, response_data = self._controller.get_response(url=url, codepage='iso-8859-1', save_to_storage=settings_model.verbose, storage='.cache/{0}-{1}.cache'.format(self.__module__, '__log_in-2'))
			if settings_model.verbose:
				yield response_headers
				yield response_data[:2000]

			try:
				self.session_id = next(x for x in response_data.splitlines() if '/Dyn?PW=' in x).split('/Dyn?PW=')[1].split('&')[0]
			except Exception as e:
				if attempts > 0:
					if settings_model.verbose:
						yield 'Can not get session id. {} attempts more...'.format(attempts)
					continue  # Try one more time from the beginning?
				else:
					raise _ProcessingException('Can not get session id')

			break

	def __load_data(self, data_format, dst_path, simulate_response=False):
		settings_model = self._settings_model

		src_re_time_format = r'^\d{2}\.\d{2}\.\d{4}\t\d{2}:\d{2}:\d{2}'
		src_datetime_format = '%d.%m.%Y\t%H:%M:%S'
		src_time_length = 19
		dst_re_time_format = r'^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}'
		dst_datetime_format = '%Y-%m-%d %H:%M:%S'
		dst_time_length = 19

		# Check if directory still exists (The remote storage is still reachable)
		self._check_if_path_exists(os.path.join(*os.path.split(dst_path)[:-1]))

		# Find dst's latest time
		try:
			with open(dst_path) as dst:
				# Accelerated search of last time
				dst.seek(0, os.SEEK_END)  # Fast-forward to the end of file
				dst.seek(max(dst.tell() - 1024, 0), 0)  # Backwards to position = -1024
				latest_time_string = next(x for x in reversed(dst.readlines()) if re.search(dst_re_time_format, x))[:dst_time_length]
		except (IOError, StopIteration):
			latest_time_string = datetime.datetime.combine(datetime.datetime.now() - datetime.timedelta(days=(9999 if simulate_response else 0)), datetime.time(0, 0, 0)).strftime(dst_datetime_format)
		latest_time = datetime.datetime.strptime(latest_time_string, dst_datetime_format)
		latest_time_string = datetime.datetime.strftime(latest_time, src_datetime_format)
		if settings_model.verbose:
			yield latest_time

		# Create request to server
		url = (
			'http://192.168.1.11/{self._load_data_script}.TXT.GZ?'  # Format: [ TXT | TXT.GZ ]
			'PW={self.session_id}'
			'&GZ=1'
		).format(**locals())
		if settings_model.verbose:
			yield url

		# Send request
		response_headers, response_data = self._controller.get_response(url=url, codepage='iso-8859-1', save_to_storage=settings_model.verbose, storage='.cache/{0}-{1}.cache'.format(self.__module__, '__load_data'))
		if settings_model.verbose:
			yield response_headers
			yield response_data[:200]
			yield response_data[-100:]
		data = response_data.splitlines()

		# Check if directory still exists (The remote storage is still reachable)
		self._check_if_path_exists(os.path.join(*os.path.split(dst_path)[:-1]))

		# Write headers if not exists
		if not os.path.exists(dst_path) or os.path.getsize(dst_path) == 0:
			with open(dst_path, 'w') as dst:
				for line in data:
					if re.search(src_re_time_format, line):
						break
					if line.startswith('Datum\tUhrzeit'):
						line = line.replace('Datum\tUhrzeit', 'Datum', 1).rstrip()
						print >>dst, (','.join(line.replace(',', '.').split('\t'))) if data_format == 'csv' else line

		with open(dst_path, 'a') as dst:
			for line in data:
				line_time_string = line[:src_time_length]
				if re.search(src_re_time_format, line_time_string):
					if line_time_string > latest_time_string:
						line_time = datetime.datetime.strptime(line[:src_time_length], src_datetime_format)
						latest_time = line_time
						line_data = line[src_time_length:]
						if len(line) > src_time_length:  # Skip lines with only time
							line_data = line_time.strftime(dst_datetime_format) + line_data
							print >>dst, (','.join(line_data.replace(',', '.').split('\t'))) if data_format == 'csv' else line_data

	def _check_if_path_exists(self, path):
		if not os.path.exists(path):
			raise IOError("Path is unreachable: {}".format(path))


class _ProcessingException(Exception):
	pass


class _PasswordEncoder(object):
	"""
		From JavaScript:

		function str2binl(str) {
			var bin = Array();
			var mask = (1 << 8) - 1;
			console.log(str.length, str.length * 8)
			for(var i = 0; i < str.length * 8; i += 8)
			//console.log(i, i >> 5)
				bin[i>>5] |= (str.charCodeAt(i / 8) & mask) << (i%32);
			console.log("BIN=" + bin)
			return bin;
		}
		function core_hmac_md5(password, challenge) {
			var bkey = str2binl(password);
			if(bkey.length > 16) bkey = core_md5(bkey, password.length * 8);
			var ipad = Array(16), opad = Array(16);
			console.log("BKEY=" + bkey)
			for(var i = 0; i < 16; i++) {
			console.log("BKEY[i]=" + i + " " + bkey[i] + " " + (bkey[i] ^ 0x36363636))
					ipad[i] = bkey[i] ^ 0x36363636;
					opad[i] = bkey[i] ^ 0x5C5C5C5C;
			}
			console.log("IPAD=" + ipad)
			console.log("OPAD=" + opad)
			var hash = core_md5(ipad.concat(str2binl(challenge)), 512 + challenge.length * 8);
			return core_md5(opad.concat(hash), 512 + 128);
		}
		function binl2hex(binarray) {
			var hex_tab = "0123456789abcdef";
			var str = "";
			for(var i = 0; i < binarray.length * 4; i++) {
					str += hex_tab.charAt((binarray[i>>2] >> ((i%4)*8+4)) & 0xF) + hex_tab.charAt((binarray[i>>2] >> ((i%4)*8  )) & 0xF);
			}
			return str;
		}
		function core_md5(x, len) {
		/* append padding */
		x[len >> 5] |= 0x80 << ((len) % 32);
		x[(((len + 64) >>> 9) << 4) + 14] = len;

		var a =  1732584193;
		var b = -271733879;
		var c = -1732584194;
		var d =  271733878;

		for(var i = 0; i < x.length; i += 16)
		{
			var olda = a;
			var oldb = b;
			var oldc = c;
			var oldd = d;

			a = md5_ff(a, b, c, d, x[i+ 0], 7 , -680876936);
			d = md5_ff(d, a, b, c, x[i+ 1], 12, -389564586);
			c = md5_ff(c, d, a, b, x[i+ 2], 17,  606105819);
			b = md5_ff(b, c, d, a, x[i+ 3], 22, -1044525330);
			a = md5_ff(a, b, c, d, x[i+ 4], 7 , -176418897);
			d = md5_ff(d, a, b, c, x[i+ 5], 12,  1200080426);
			c = md5_ff(c, d, a, b, x[i+ 6], 17, -1473231341);
			b = md5_ff(b, c, d, a, x[i+ 7], 22, -45705983);
			a = md5_ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
			d = md5_ff(d, a, b, c, x[i+ 9], 12, -1958414417);
			c = md5_ff(c, d, a, b, x[i+10], 17, -42063);
			b = md5_ff(b, c, d, a, x[i+11], 22, -1990404162);
			a = md5_ff(a, b, c, d, x[i+12], 7 ,  1804603682);
			d = md5_ff(d, a, b, c, x[i+13], 12, -40341101);
			c = md5_ff(c, d, a, b, x[i+14], 17, -1502002290);
			b = md5_ff(b, c, d, a, x[i+15], 22,  1236535329);

			a = md5_gg(a, b, c, d, x[i+ 1], 5 , -165796510);
			d = md5_gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
			c = md5_gg(c, d, a, b, x[i+11], 14,  643717713);
			b = md5_gg(b, c, d, a, x[i+ 0], 20, -373897302);
			a = md5_gg(a, b, c, d, x[i+ 5], 5 , -701558691);
			d = md5_gg(d, a, b, c, x[i+10], 9 ,  38016083);
			c = md5_gg(c, d, a, b, x[i+15], 14, -660478335);
			b = md5_gg(b, c, d, a, x[i+ 4], 20, -405537848);
			a = md5_gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
			d = md5_gg(d, a, b, c, x[i+14], 9 , -1019803690);
			c = md5_gg(c, d, a, b, x[i+ 3], 14, -187363961);
			b = md5_gg(b, c, d, a, x[i+ 8], 20,  1163531501);
			a = md5_gg(a, b, c, d, x[i+13], 5 , -1444681467);
			d = md5_gg(d, a, b, c, x[i+ 2], 9 , -51403784);
			c = md5_gg(c, d, a, b, x[i+ 7], 14,  1735328473);
			b = md5_gg(b, c, d, a, x[i+12], 20, -1926607734);

			a = md5_hh(a, b, c, d, x[i+ 5], 4 , -378558);
			d = md5_hh(d, a, b, c, x[i+ 8], 11, -2022574463);
			c = md5_hh(c, d, a, b, x[i+11], 16,  1839030562);
			b = md5_hh(b, c, d, a, x[i+14], 23, -35309556);
			a = md5_hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
			d = md5_hh(d, a, b, c, x[i+ 4], 11,  1272893353);
			c = md5_hh(c, d, a, b, x[i+ 7], 16, -155497632);
			b = md5_hh(b, c, d, a, x[i+10], 23, -1094730640);
			a = md5_hh(a, b, c, d, x[i+13], 4 ,  681279174);
			d = md5_hh(d, a, b, c, x[i+ 0], 11, -358537222);
			c = md5_hh(c, d, a, b, x[i+ 3], 16, -722521979);
			b = md5_hh(b, c, d, a, x[i+ 6], 23,  76029189);
			a = md5_hh(a, b, c, d, x[i+ 9], 4 , -640364487);
			d = md5_hh(d, a, b, c, x[i+12], 11, -421815835);
			c = md5_hh(c, d, a, b, x[i+15], 16,  530742520);
			b = md5_hh(b, c, d, a, x[i+ 2], 23, -995338651);

			a = md5_ii(a, b, c, d, x[i+ 0], 6 , -198630844);
			d = md5_ii(d, a, b, c, x[i+ 7], 10,  1126891415);
			c = md5_ii(c, d, a, b, x[i+14], 15, -1416354905);
			b = md5_ii(b, c, d, a, x[i+ 5], 21, -57434055);
			a = md5_ii(a, b, c, d, x[i+12], 6 ,  1700485571);
			d = md5_ii(d, a, b, c, x[i+ 3], 10, -1894986606);
			c = md5_ii(c, d, a, b, x[i+10], 15, -1051523);
			b = md5_ii(b, c, d, a, x[i+ 1], 21, -2054922799);
			a = md5_ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
			d = md5_ii(d, a, b, c, x[i+15], 10, -30611744);
			c = md5_ii(c, d, a, b, x[i+ 6], 15, -1560198380);
			b = md5_ii(b, c, d, a, x[i+13], 21,  1309151649);
			a = md5_ii(a, b, c, d, x[i+ 4], 6 , -145523070);
			d = md5_ii(d, a, b, c, x[i+11], 10, -1120210379);
			c = md5_ii(c, d, a, b, x[i+ 2], 15,  718787259);
			b = md5_ii(b, c, d, a, x[i+ 9], 21, -343485551);

			a = safe_add(a, olda);
			b = safe_add(b, oldb);
			c = safe_add(c, oldc);
			d = safe_add(d, oldd);
		}
		return Array(a, b, c, d);

		}
		function md5_ff(a, b, c, d, x, s, t) {
		return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
		}

		function md5_ii(a, b, c, d, x, s, t) {
		return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
		}
		function md5_cmn(q, a, b, x, s, t) {
		return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s),b);
		}
		function safe_add(x, y) {
		var lsw = (x & 0xFFFF) + (y & 0xFFFF);
		var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
		return (msw << 16) | (lsw & 0xFFFF);
		}
		function bit_rol(num, cnt) {
		return (num << cnt) | (num >>> (32 - cnt));
		}
		function md5_gg(a, b, c, d, x, s, t) {
		return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
		}
		function md5_hh(a, b, c, d, x, s, t) {
		return md5_cmn(b ^ c ^ d, a, b, x, s, t);
		}

		var challenge = "7c22e3f61bd9107184844e46c8ee2b3e88ccf9f539770b310e93c8c66bbbb0b6"
		var password = "nivus"
		binl2hex(core_hmac_md5(password, challenge))
	"""

	hexcase = 0
	b64pad = ''
	chrsz = 8
	mode = 32

	@classmethod
	def str2binl(cls, D):
		C = []
		A = (1 << cls.chrsz) - 1
		for B in range(0, len(D) * cls.chrsz, cls.chrsz):
			if len(C) == (B >> 5):
				C.append(0)
			C[B >> 5] |= (ord(D[B // cls.chrsz]) & A) << (B % 32)
		return C

	@classmethod
	def binl2hex(cls, C):
		B = "0123456789abcdef"
		if cls.hexcase:
			B = "0123456789ABCDEF"
		D = ''
		for A in range(0, len(C) * 4, 1):
			D += B[(C[A >> 2] >> ((A % 4) * 8 + 4)) & 15] + B[(C[A >> 2] >> ((A % 4) * 8)) & 15]
		return D

	@classmethod
	def md5(cls, A):
		return cls.hex_md5(A)

	@classmethod
	def hex_md5(cls, A):
		return cls.binl2hex(cls.core_md5(cls.str2binl(A), len(A) * cls.chrsz))

	@classmethod
	def str_md5(cls, A):
		return cls.binl2str(cls.core_md5(cls.str2binl(A), len(A) * cls.chrsz))

	@classmethod
	def hex_hmac_md5(cls, A, B):
		return cls.binl2hex(cls.core_hmac_md5(A, B))

	@classmethod
	def b64_hmac_md5(cls, A, B):
		return binl2b64(core_hmac_md5(A, B))

	@classmethod
	def str_hmac_md5(cls, A, B):
		return cls.binl2str(cls.core_hmac_md5(A, B))

	@classmethod
	def md5_vm_test(cls):
		return cls.hex_md5('abc') == '900150983cd24fb0d6963f7d28e17f72'

	# this is equvalent of >> > in js
	@classmethod
	def rsl(cls, A, B):
		return (A & 0xFFFFFFFFL) >> B

	@classmethod
	def jsArraySet(cls, list, index, value):
		if index < 0:
			return
		if len(list) > index:
			list[index] = value
		else:
			for i in range(index - len(list)):
				list.append(0)
			list.append(value)

	@classmethod
	def jsArrayGet(cls, list, index):
		if len(list) > index:
			return list[index]
		else:
			return 0

	@classmethod
	def core_md5(cls, K, F):
		#K[F >> 5] |= 128 << ((F) % 32)
		cls.jsArraySet(K, F >> 5, cls.jsArrayGet(K, F >> 5) | (128 << (F % 32)))
		#K[(cls.rsl(F + 64, 9) << 4) + 14] = F
		cls.jsArraySet(K, (cls.rsl(F + 64, 9) << 4) + 14, F)
		J = 1732584193
		I = -271733879
		H = -1732584194
		G = 271733878

		for C in range(0, len(K), 16):
			E = J
			D = I
			B = H
			A = G
			J = cls.md5_ff(J, I, H, G, cls.jsArrayGet(K, C + 0), 7, -680876936)
			G = cls.md5_ff(G, J, I, H, cls.jsArrayGet(K, C + 1), 12, -389564586)
			H = cls.md5_ff(H, G, J, I, cls.jsArrayGet(K, C + 2), 17, 606105819)
			I = cls.md5_ff(I, H, G, J, cls.jsArrayGet(K, C + 3), 22, -1044525330)
			J = cls.md5_ff(J, I, H, G, cls.jsArrayGet(K, C + 4), 7, -176418897)
			G = cls.md5_ff(G, J, I, H, cls.jsArrayGet(K, C + 5), 12, 1200080426)
			H = cls.md5_ff(H, G, J, I, cls.jsArrayGet(K, C + 6), 17, -1473231341)
			I = cls.md5_ff(I, H, G, J, cls.jsArrayGet(K, C + 7), 22, -45705983)
			J = cls.md5_ff(J, I, H, G, cls.jsArrayGet(K, C + 8), 7, 1770035416)
			G = cls.md5_ff(G, J, I, H, cls.jsArrayGet(K, C + 9), 12, -1958414417)
			H = cls.md5_ff(H, G, J, I, cls.jsArrayGet(K, C + 10), 17, -42063)
			I = cls.md5_ff(I, H, G, J, cls.jsArrayGet(K, C + 11), 22, -1990404162)
			J = cls.md5_ff(J, I, H, G, cls.jsArrayGet(K, C + 12), 7, 1804603682)
			G = cls.md5_ff(G, J, I, H, cls.jsArrayGet(K, C + 13), 12, -40341101)
			H = cls.md5_ff(H, G, J, I, cls.jsArrayGet(K, C + 14), 17, -1502002290)
			I = cls.md5_ff(I, H, G, J, cls.jsArrayGet(K, C + 15), 22, 1236535329)
			J = cls.md5_gg(J, I, H, G, cls.jsArrayGet(K, C + 1), 5, -165796510)
			G = cls.md5_gg(G, J, I, H, cls.jsArrayGet(K, C + 6), 9, -1069501632)
			H = cls.md5_gg(H, G, J, I, cls.jsArrayGet(K, C + 11), 14, 643717713)
			I = cls.md5_gg(I, H, G, J, cls.jsArrayGet(K, C + 0), 20, -373897302)
			J = cls.md5_gg(J, I, H, G, cls.jsArrayGet(K, C + 5), 5, -701558691)
			G = cls.md5_gg(G, J, I, H, cls.jsArrayGet(K, C + 10), 9, 38016083)
			H = cls.md5_gg(H, G, J, I, cls.jsArrayGet(K, C + 15), 14, -660478335)
			I = cls.md5_gg(I, H, G, J, cls.jsArrayGet(K, C + 4), 20, -405537848)
			J = cls.md5_gg(J, I, H, G, cls.jsArrayGet(K, C + 9), 5, 568446438)
			G = cls.md5_gg(G, J, I, H, cls.jsArrayGet(K, C + 14), 9, -1019803690)
			H = cls.md5_gg(H, G, J, I, cls.jsArrayGet(K, C + 3), 14, -187363961)
			I = cls.md5_gg(I, H, G, J, cls.jsArrayGet(K, C + 8), 20, 1163531501)
			J = cls.md5_gg(J, I, H, G, cls.jsArrayGet(K, C + 13), 5, -1444681467)
			G = cls.md5_gg(G, J, I, H, cls.jsArrayGet(K, C + 2), 9, -51403784)
			H = cls.md5_gg(H, G, J, I, cls.jsArrayGet(K, C + 7), 14, 1735328473)
			I = cls.md5_gg(I, H, G, J, cls.jsArrayGet(K, C + 12), 20, -1926607734)
			J = cls.md5_hh(J, I, H, G, cls.jsArrayGet(K, C + 5), 4, -378558)
			G = cls.md5_hh(G, J, I, H, cls.jsArrayGet(K, C + 8), 11, -2022574463)
			H = cls.md5_hh(H, G, J, I, cls.jsArrayGet(K, C + 11), 16, 1839030562)
			I = cls.md5_hh(I, H, G, J, cls.jsArrayGet(K, C + 14), 23, -35309556)
			J = cls.md5_hh(J, I, H, G, cls.jsArrayGet(K, C + 1), 4, -1530992060)
			G = cls.md5_hh(G, J, I, H, cls.jsArrayGet(K, C + 4), 11, 1272893353)
			H = cls.md5_hh(H, G, J, I, cls.jsArrayGet(K, C + 7), 16, -155497632)
			I = cls.md5_hh(I, H, G, J, cls.jsArrayGet(K, C + 10), 23, -1094730640)
			J = cls.md5_hh(J, I, H, G, cls.jsArrayGet(K, C + 13), 4, 681279174)
			G = cls.md5_hh(G, J, I, H, cls.jsArrayGet(K, C + 0), 11, -358537222)
			H = cls.md5_hh(H, G, J, I, cls.jsArrayGet(K, C + 3), 16, -722521979)
			I = cls.md5_hh(I, H, G, J, cls.jsArrayGet(K, C + 6), 23, 76029189)
			J = cls.md5_hh(J, I, H, G, cls.jsArrayGet(K, C + 9), 4, -640364487)
			G = cls.md5_hh(G, J, I, H, cls.jsArrayGet(K, C + 12), 11, -421815835)
			H = cls.md5_hh(H, G, J, I, cls.jsArrayGet(K, C + 15), 16, 530742520)
			I = cls.md5_hh(I, H, G, J, cls.jsArrayGet(K, C + 2), 23, -995338651)
			J = cls.md5_ii(J, I, H, G, cls.jsArrayGet(K, C + 0), 6, -198630844)
			G = cls.md5_ii(G, J, I, H, cls.jsArrayGet(K, C + 7), 10, 1126891415)
			H = cls.md5_ii(H, G, J, I, cls.jsArrayGet(K, C + 14), 15, -1416354905)
			I = cls.md5_ii(I, H, G, J, cls.jsArrayGet(K, C + 5), 21, -57434055)
			J = cls.md5_ii(J, I, H, G, cls.jsArrayGet(K, C + 12), 6, 1700485571)
			G = cls.md5_ii(G, J, I, H, cls.jsArrayGet(K, C + 3), 10, -1894986606)
			H = cls.md5_ii(H, G, J, I, cls.jsArrayGet(K, C + 10), 15, -1051523)
			I = cls.md5_ii(I, H, G, J, cls.jsArrayGet(K, C + 1), 21, -2054922799)
			J = cls.md5_ii(J, I, H, G, cls.jsArrayGet(K, C + 8), 6, 1873313359)
			G = cls.md5_ii(G, J, I, H, cls.jsArrayGet(K, C + 15), 10, -30611744)
			H = cls.md5_ii(H, G, J, I, cls.jsArrayGet(K, C + 6), 15, -1560198380)
			I = cls.md5_ii(I, H, G, J, cls.jsArrayGet(K, C + 13), 21, 1309151649)
			J = cls.md5_ii(J, I, H, G, cls.jsArrayGet(K, C + 4), 6, -145523070)
			G = cls.md5_ii(G, J, I, H, cls.jsArrayGet(K, C + 11), 10, -1120210379)
			H = cls.md5_ii(H, G, J, I, cls.jsArrayGet(K, C + 2), 15, 718787259)
			I = cls.md5_ii(I, H, G, J, cls.jsArrayGet(K, C + 9), 21, -343485551)
			J = cls.safe_add(J, E)
			I = cls.safe_add(I, D)
			H = cls.safe_add(H, B)
			G = cls.safe_add(G, A)

		if cls.mode == 16:
			return [I, H]
		else:
			return [J, I, H, G]

	@classmethod
	def md5_cmn(cls, F, C, B, A, E, D):
		return cls.safe_add(cls.bit_rol(cls.safe_add(cls.safe_add(C, F), cls.safe_add(A, D)), E), B)

	@classmethod
	def md5_ff(cls, C, B, G, F, A, E, D):
		return cls.md5_cmn((B & G) | ((~B) & F), C, B, A, E, D)

	@classmethod
	def md5_gg(cls, C, B, G, F, A, E, D):
		return cls.md5_cmn((B & F) | (G & (~F)), C, B, A, E, D)

	@classmethod
	def md5_hh(cls, C, B, G, F, A, E, D):
		return cls.md5_cmn(B ^ G ^ F, C, B, A, E, D)

	@classmethod
	def md5_ii(cls, C, B, G, F, A, E, D):
		return cls.md5_cmn(G ^ (B | (~F)), C, B, A, E, D)

	@classmethod
	def core_hmac_md5(cls, C, F):
		E = cls.str2binl(C)
		if len(E) > 16:
			E = cls.core_md5(E, len(C) * cls.chrsz)
		A = [None] * 16
		D = [None] * 16
		for B in range(0, 16):
			A[B] = (E[B] if len(E) > B else 0) ^ 0x36363636
			D[B] = (E[B] if len(E) > B else 0) ^ 0x5c5c5c5c
		G = cls.core_md5(A + cls.str2binl(F), 512 + len(F) * cls.chrsz)
		return cls.core_md5(D + G, 512 + 128)

	@classmethod
	def safe_add(cls, A, D):
		C = (A & 65535) + (D & 65535)
		B = (A >> 16) + (D >> 16) + (C >> 16)
		return (B << 16) | (C & 65535)

	@classmethod
	def bit_rol(cls, A, B):
		return (A << B) | cls.rsl(A, 32 - B)

	@classmethod
	def binl2str(cls, C):
		D = ''
		A = (1 << cls.chrsz) - 1
		for B in range(0, len(C) * 32, cls.chrsz):
			D += chr(cls.rsl(C[B >> 5], B % 32) & A)
		return D

	@classmethod
	def binl2b64(cls, D):
		C = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
		F = ''
		for B in range(0, len(D) * 4, 3):
			E = (((D[B >> 2] >> 8 * (B % 4)) & 255) << 16) | (((D[B + 1 >> 2] >> 8 * ((B + 1) % 4)) & 255) << 8) | ((D[B + 2 >> 2] >> 8 * ((B + 2) % 4)) & 255)
			for A in range(0, 4):
				if B * 8 + A * 6 > len(D) * 32:
					F += cls.b64pad
				else:
					F += C[(E >> 6 * (3 - A)) & 63]
		return F

	@classmethod
	def hexchar2bin(cls, str):
		arr = []
		for i in range(0, len(str), 2):
			arr.append("\\x" + str[i:i + 2])
		arr = "".join(arr)
		exec("temp = '" + arr + "'")
		return temp

	@classmethod
	def md5_3(cls, B):
		hash = cls.core_md5(cls.str2binl(B), len(B) * cls.chrsz)
		hash = cls.core_md5(hash, 16 * cls.chrsz)
		hash = cls.core_md5(hash, 16 * cls.chrsz)
		return cls.binl2hex(hash)


def run_encrypt_password():
	password = 'nivus'
	challenge = '7c22e3f61bd9107184844e46c8ee2b3e88ccf9f539770b310e93c8c66bbbb0b6'
	awaited_result = 'e5d74aff0cbbd867a86c126dce3166a3'
	result = _PasswordEncoder.hex_hmac_md5(password, challenge)
	print >>sys.stderr, "result, awaited_result, result == awaited_result,", result, awaited_result, result == awaited_result; sys.stderr.flush()


def main():
	run_encrypt_password()

if __name__ == '__main__':
	main()
