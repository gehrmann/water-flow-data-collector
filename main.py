#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann (gehrmann.mail@gmail.com)

from __future__ import division, unicode_literals
import os
import signal; signal.signal(signal.SIGINT, signal.default_int_handler)  # Working Ctrl-C for QT
import sys

from PyQt4 import QtGui

if __name__ == '__main__':
	reload(sys); sys.setdefaultencoding('utf-8')
	os.chdir(os.path.dirname(sys.executable if getattr(sys, "frozen", False) else __file__) or '.'); sys.path.insert(0, os.path.realpath(os.getcwd()))

from controllers.main import MainController


def main():
	app = QtGui.QApplication(sys.argv)

	# print [unicode(x) for x in QtGui.QStyleFactory.keys()]
	# app.setStyle(QtGui.QStyleFactory.create('Cleanlooks'))
	# app.setStyle(QtGui.QStyleFactory.create([unicode(x) for x in QtGui.QStyleFactory.keys()][0]))

	MainController.init()

	sys.exit(app.exec_())

if __name__ == '__main__':
	main()
